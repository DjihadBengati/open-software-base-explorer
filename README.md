# open-software-base-explorer
Serve [Open Software Base](https://git.framasoft.org/codegouv/open-software-base-yaml) in JSON and HTML format.

## Install

Download [this program](https://git.framasoft.org/codegouv/open-software-base-explorer):
```bash
git clone https://git.framasoft.org/codegouv/open-software-base-explorer.git

Djihad BENGATI repository :
https://framagit.org/DjihadBengati/open-software-base-explorer.git
```


Install dependencies:
```bash
cd open-software-base-explorer/
npm install
npm run build
node index.js # server running on http://localhost:3000
```

The [Open Software Base (in YAML format)](https://git.framasoft.org/codegouv/open-software-base-yaml) is included in the dependencies. It is cached on first access, so you need to restart the process when updating database.

[Read more](https://git.framasoft.org/codegouv/open-software-base-explorer/blob/master/doc/workflow.md) about the data sources of this project.



## Development

In development, you can watch directories to instantly rebuild pages and stylesheets upon modification.

To monitor stylesheets:
```bash
npm run build:styles:watch
```

To monitor javascript:
```bash
npm run build:javascript:watch
```
